﻿using System;
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WinPage : MonoBehaviour
    {
        [SerializeField] private TMP_Text _text = null;
        [SerializeField] private Button _restartButton = null;

        public event Action RestartClicked;
        private void Awake()
        {
            _restartButton.onClick.AddListener(() => RestartClicked?.Invoke());
        }

        public void Open(TeamType winner)
        {
            _text.text = $"{winner} Won!";
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
