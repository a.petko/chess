﻿using System;
using Core;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class ReturnCharacterPage : MonoBehaviour
    {
        [SerializeField] private Button _queenButton = null;
        [SerializeField] private Button _bishopButton = null;
        [SerializeField] private Button _rookButton = null;
        [SerializeField] private Button _knightButton = null;

        public event Action<CharacterType> CharacterSelected;

        private void Awake()
        {
            _queenButton.onClick.AddListener(
                () => CharacterSelected?.Invoke(CharacterType.Queen));
            _bishopButton.onClick.AddListener(
                () => CharacterSelected?.Invoke(CharacterType.Bishop));
            _rookButton.onClick.AddListener(
                () => CharacterSelected?.Invoke(CharacterType.Rook));
            _knightButton.onClick.AddListener(
                () => CharacterSelected?.Invoke(CharacterType.Knight));
        }

        public void Open()
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}