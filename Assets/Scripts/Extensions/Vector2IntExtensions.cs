﻿using UnityEngine;

namespace Extensions
{
    public static class Vector2IntExtensions
    {
        public static void Deconstruct(this Vector2Int vector, out int x, out int y)
        {
            x = vector.x;
            y = vector.y;
        }
    }
}