﻿using System;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace FsmSystem
{
    public class Fsm<TS, TE> where TS : class, IState
    {
        private readonly IEqualityComparer<TE> _comparer;
        private readonly Dictionary<TE, TS> _globalMap;
        private readonly Dictionary<TS, Dictionary<TE, TS>> _translationMap;

        public TS Current { get; private set; }

        public Fsm(
            int capacity = 0, 
            IEqualityComparer<TE> comparer = null, 
            IEqualityComparer<TS> addedComparer = null)
        {
            _comparer = comparer;
            _globalMap = new Dictionary<TE, TS>(comparer);
            _translationMap = new Dictionary<TS, Dictionary<TE, TS>>(capacity, addedComparer);
        }
        public Fsm<TS, TE> Add(TS from, TS to, TE e)
        {
            Assert.IsNotNull(from);
            Assert.IsNotNull(to);
            
            if (!_translationMap.ContainsKey(from))
                _translationMap.Add(from, new Dictionary<TE, TS>(_comparer));

            var eventMap = _translationMap[from];
            if (eventMap.ContainsKey(e))
                throw new ArgumentException(
                    "the collection already contains these arguments");

            eventMap[e] = to;
            return this;
        }

        public Fsm<TS, TE> AddGlobal(TS to, TE e)
        {
            Assert.IsNotNull(to);
            
            if (_globalMap.ContainsKey(e))
                throw new ArgumentException(
                    "the collection already contains these arguments");

            _globalMap[e] = to;
            return this;
        }
        
        public void Start(TS state)
        {
            Assert.IsNotNull(state);

            Current = state;
            Current.Enter();
        }

        public void Fire(TE e)
        {
            if (!_translationMap.ContainsKey(Current) || !_translationMap[Current].ContainsKey(e))
            {
                if (_globalMap.ContainsKey(e) && !Current.Equals(_globalMap[e]))
                {
                    Current.Exit();
                    Current = _globalMap[e];
                    Current.Enter();
                }
                return;
            }

            Current.Exit();
            Current = _translationMap[Current][e];
            Current.Enter();
        }
    }
}