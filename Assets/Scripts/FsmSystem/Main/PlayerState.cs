﻿using System;
using Board;
using Core;
using UnityEngine;

namespace FsmSystem.Main
{
    public sealed class PlayerState : State
    {
        private readonly TeamType _team;
        private readonly GameSession _gameSession;
        private readonly Camera _camera;
        private readonly BoardController _board;

        public event Action Moved;
        public event Action GameOvered;

        public PlayerState(Camera camera, TeamType team, GameSession gameSession, BoardController board)
        {
            _camera = camera;
            _team = team;
            _gameSession = gameSession;
            _board = board;
        }

        public override void Enter()
        {
            _gameSession.CurrentTeam = _team;
            _board.UpdateMoveCache(_team);
            if (!_board.HasPossibleMove())
                GameOvered?.Invoke();
        }

        public override void Update()
        {
            if (!Input.GetMouseButtonDown(0))
                return;

            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out var hit))
                return;

            var index = _board.GetIndex(hit.point);
            var current = _gameSession.CurrentIndex;

            if (current != null && _board.IsPossibleMove(current.Value, index))
            {
                _board.Move(current.Value, index);
                _gameSession.CurrentIndex = null;
                Moved?.Invoke();
                return;
            }

            if (_board.TryGetTeam(index, out var t) && t == _team)
                _gameSession.CurrentIndex = index;
        }
    }
}