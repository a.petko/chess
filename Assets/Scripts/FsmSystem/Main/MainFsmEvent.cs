﻿namespace FsmSystem.Main
{
    public enum MainFsmEvent
    {
        Idle,
        WhiteState,
        BlackState,
        PawnTransformation
    }
}