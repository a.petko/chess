﻿using System;
using Board;
using Core;
using UI;
using UnityEngine.Assertions;

namespace FsmSystem.Main
{
    public sealed class SelectCharacterState : State
    {
        private readonly ReturnCharacterPage _returnCharacterPage;
        private readonly BoardController _board;
        private readonly GameSession _gameSession;

        public event Action Completed;
    
        public SelectCharacterState(
            ReturnCharacterPage returnCharacterPage, BoardController board, GameSession gameSession)
        {
            _returnCharacterPage = returnCharacterPage;
            _board = board;
            _gameSession = gameSession;
        }
    
        public override void Enter()
        {
            _returnCharacterPage.CharacterSelected += OnCharacterSelected;
            _returnCharacterPage.Open();
        }

        public override void Exit()
        {
            _returnCharacterPage.CharacterSelected -= OnCharacterSelected;
            _returnCharacterPage.Close();
        }
    
        private void OnCharacterSelected(CharacterType characterType)
        {
            Assert.IsTrue(_gameSession.PawnTransformationReachedIndex != null);
        
            var index = _gameSession.PawnTransformationReachedIndex.Value;
            _board.TryGetTeam(index, out var teamType);
            _board.SetCharacter(index, characterType, teamType);
            _gameSession.PawnTransformationReachedIndex = null;
            Completed?.Invoke();
        }
    }
}