﻿using System.Collections.Generic;

namespace FsmSystem.Main
{
    public sealed class MainFsmEventIEqualityComparer : IEqualityComparer<MainFsmEvent>
    {
        public bool Equals(MainFsmEvent x, MainFsmEvent y) => x == y;

        public int GetHashCode(MainFsmEvent obj) => (int) obj;
    }
}