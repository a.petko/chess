﻿namespace FsmSystem.Main
{
    public class State : IState
    {
        public virtual void Enter()
        {
            // for override
        }

        public virtual void Update()
        {
            // for override
        }

        public virtual void Exit()
        {
            // for override
        }
    }
}