﻿using System.Collections.Generic;
using Core;
using UnityEngine;

namespace Board
{
    public sealed class DebugBoard : MonoBehaviour
    {
        [SerializeField] private BoardRender _renderer = null;
        [SerializeField] private Vector2Int _size = new Vector2Int(8, 8);
        [SerializeField] private Vector2 _cellSize = Vector2.one;
        
        private readonly List<Character> _characters = new List<Character>();

        private CharacterBuilder _characterBuilder;
        private ChessEngine _record;
        
        public void Init(CharacterBuilder characterBuilder, ChessEngine record)
        {
            _characterBuilder = characterBuilder;
            _record = record;
            _renderer.Init(
                new Vector2Int(8, 8), 
                Vector2.one, 
                Color.magenta, 
                Color.yellow, 
                Color.green);
        }

        private void Update()
        {
            if (_record == null)
                return;

            foreach (var c in _characters)
            {
                Destroy(c.gameObject);
            }
        
            _characters.Clear();
        
            foreach (var (index, characterType, teamType) in _record.GetAllCharacters())
                SetCharacterWithoutRecord(index, characterType, teamType);
        }
    
        private void SetCharacterWithoutRecord(
            in Vector2Int index, CharacterType characterType, TeamType teamType)
        {
            var character = _characterBuilder
                .SetCharacter(characterType)
                .SetTeam(teamType)
                .Build();
        
            character.transform.position = GetPosition(index);
            _characters.Add(character);
        }
    
        private Vector3 GetPosition(in Vector2Int index)
        {
            var x = index.x * _cellSize.x;
            var y = index.y * _cellSize.y;
            var offset = _size - Vector2.one;
            offset *= 0.5f * _cellSize;
            return transform.position + new Vector3(x - offset.x, 0f, y - offset.y);
        }
    }
}