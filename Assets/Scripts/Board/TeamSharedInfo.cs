﻿
using UnityEngine;

namespace Board
{
    public sealed class TeamSharedInfo
    {
        public readonly Color MainColor;
        public readonly Quaternion Rotation;

        public TeamSharedInfo(Color mainColor, Quaternion rotation)
        {
            MainColor = mainColor;
            Rotation = rotation;
        }
    }
}