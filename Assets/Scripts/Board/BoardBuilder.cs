﻿using System;
using Core;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Board
{
    [Serializable]
    public sealed class BoardBuilder
    {
        [SerializeField] private BoardController _prefab = null;
        [SerializeField] private Vector2 _cellSize = Vector2.one;
        [SerializeField] private Color _color0 = Color.white;
        [SerializeField] private Color _color1 = Color.black;
        [SerializeField] private Color _selectedColor = Color.green;

        private ChessEngine _engine;
        private CharacterBuilder _characterBuilder;

        public BoardBuilder SetChessEngine(ChessEngine engine)
        {
            _engine = engine;
            return this;
        }
    
        public BoardBuilder SetCharacterBuilder(CharacterBuilder builder)
        {
            _characterBuilder = builder;
            return this;
        }
    
        public BoardBuilder SetCellSize(in Vector2 size)
        {
            _cellSize = size;
            return this;
        }
    
        public BoardBuilder SetColor0(in Color color)
        {
            _color0 = color;
            return this;
        }
    
        public BoardBuilder SetColor1(in Color color)
        {
            _color1 = color;
            return this;
        }
    
        public BoardBuilder SetSelectedColor(in Color color)
        {
            _selectedColor = color;
            return this;
        }

        public BoardController Build()
        {
            var instance = Object.Instantiate(_prefab);
            instance.Init(
                _characterBuilder,
                _engine,  
                _cellSize, 
                _color0, 
                _color1, 
                _selectedColor);
        
            return instance;
        }
    }
}