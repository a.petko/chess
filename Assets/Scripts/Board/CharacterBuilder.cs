﻿using System;
using System.Collections.Generic;
using Core;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Board
{
    [Serializable]
    public sealed class CharacterBuilder
    {
        [SerializeField] private Character _pawn = null;
        [SerializeField] private Character _bishop = null;
        [SerializeField] private Character _king = null;
        [SerializeField] private Character _knight = null;
        [SerializeField] private Character _queen = null;
        [SerializeField] private Character _rook = null;
        [SerializeField] private CharacterType _character;
        [SerializeField] private TeamType _team;

        private Dictionary<TeamType, TeamSharedInfo> _teamInfos;

        public CharacterBuilder(Dictionary<TeamType, TeamSharedInfo> teamInfos)
        {
            _teamInfos = teamInfos;
        }

        public CharacterBuilder SetTeamInfos(Dictionary<TeamType, TeamSharedInfo> teamInfos)
        {
            _teamInfos = teamInfos;
            return this;
        }
    
        public CharacterBuilder SetCharacter(CharacterType character)
        {
            _character = character;
            return this;
        }
    
        public CharacterBuilder SetTeam(TeamType team)
        {
            _team = team;
            return this;
        }
    
    
        public Character Build()
        {
            var character = Object.Instantiate(Get(_character));
            character.Init(_teamInfos[_team]);
            return character;
        }

        private Character Get(CharacterType character)
        {
            switch (character)
            {
                case CharacterType.King: return _king;
                case CharacterType.Queen: return _queen;
                case CharacterType.Bishop: return _bishop;
                case CharacterType.Rook: return _rook;
                case CharacterType.Knight: return _knight;
                case CharacterType.Pawn: return _pawn;
                default:
                    throw new ArgumentOutOfRangeException(nameof(character), character, null);
            }
        }
    }
}