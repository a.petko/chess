﻿using UnityEngine;

namespace Board
{
    public class Character : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _meshRenderer = null;
    
        private static readonly int ColorId = Shader.PropertyToID("_BaseColor");
        private static MaterialPropertyBlock _propertyBlock;
    
        private TeamSharedInfo _teamSharedInfo;
    
        private static MaterialPropertyBlock PropertyBlock =>
            _propertyBlock ?? (_propertyBlock = new MaterialPropertyBlock());

        public void Init(TeamSharedInfo teamSharedInfo)
        {
            _teamSharedInfo = teamSharedInfo;
            transform.localRotation = teamSharedInfo.Rotation;
            SetColor(_teamSharedInfo.MainColor);
        }

        private void SetColor(in Color color)
        {
            PropertyBlock.SetColor(ColorId, color);
            _meshRenderer.SetPropertyBlock(PropertyBlock);
        }
    }
}
