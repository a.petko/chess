﻿using Unity.Collections;
using UnityEngine;

namespace Board
{
    public sealed class BoardRender : MonoBehaviour
    {
        [SerializeField] private Mesh _mesh = null;
        [SerializeField] private Material _material = null;

        private static readonly int MatricesId = Shader.PropertyToID("_Matrices");
        private static readonly int ColorId = Shader.PropertyToID("_Color");

        private static MaterialPropertyBlock _propertyBlock;

        private bool _isInited;
    
        private Vector2Int _size;
        private Vector2 _cellSize;
        private Color _color0;
        private Color _color1;
        private Color _selectColor;

        private NativeArray<Matrix4x4>[] _matrices;
        private ComputeBuffer[] _matricesBuffer;

        private NativeArray<Matrix4x4> _selectMatrix;
        private ComputeBuffer _selectMatrixBuffer;

        private void Awake()
        {
            if (_propertyBlock == null)
                _propertyBlock = new MaterialPropertyBlock();
        }

        private void OnEnable()
        {
            if (!_isInited)
                return;

            OnDisable();
            const int stride = 16 * 4;
            var length = _size.x * _size.y;
            _matricesBuffer = new ComputeBuffer[2];
            _matrices = new NativeArray<Matrix4x4>[2];
        
            for (var i = 0; i < 2; ++i)
            {
                _matricesBuffer[i] = new ComputeBuffer(length, stride);
                _matrices[i] = new NativeArray<Matrix4x4>(length, Allocator.Persistent);
            }

            var tran = transform;
            var position = tran.position - new Vector3(
                0.5f * (_size.x - 1) * _cellSize.x, 0f,
                0.5f * (_size.y - 1) * _cellSize.y);
            var rotation = tran.rotation;
            var scale = tran.lossyScale;

            for (var x = 0; x < _size.x; ++x)
            for (var y = 0; y < _size.y; ++y)
            {
                var pos = position 
                          + new Vector3(x * _cellSize.x, -0.5f, y * _cellSize.y);
                _matrices[(x + y) % 2][x * _size.x + y] = Matrix4x4.TRS(pos, rotation, scale);
            }

            _matricesBuffer[0].SetData(_matrices[0]);
            _matricesBuffer[1].SetData(_matrices[1]);
        }
    
        private void LateUpdate()
        {
            if (_matricesBuffer != null)
            {
                for (var i = 0; i < _matricesBuffer.Length; ++i)
                {
                    var buffer = _matricesBuffer[i];
                    _propertyBlock.SetColor(ColorId, i == 0 ? _color0 : _color1);
                    _propertyBlock.SetBuffer(MatricesId, buffer);
                    _material.SetBuffer(MatricesId, buffer);
                    var bounds = new Bounds(transform.position,
                        new Vector3(_size.x * _cellSize.x, 1f, _size.y * _cellSize.y));
                    Graphics.DrawMeshInstancedProcedural(
                        _mesh, 0, _material, bounds, buffer.count, _propertyBlock);
                }
            }

            if (_selectMatrixBuffer != null)
            {
                _propertyBlock.SetColor(ColorId, _selectColor);
                _propertyBlock.SetBuffer(MatricesId, _selectMatrixBuffer);
                _material.SetBuffer(MatricesId, _selectMatrixBuffer);
                var bounds = new Bounds(transform.position,
                    new Vector3(_size.x * _cellSize.x, 1f, _size.y * _cellSize.y));
                Graphics.DrawMeshInstancedProcedural(
                    _mesh, 0, _material, bounds, _selectMatrixBuffer.count, _propertyBlock);
            }
        }

        private void OnDisable()
        {
            Deselect();
            if (_matricesBuffer == null)
                return;

            for (var i = 0; i < _matricesBuffer.Length; ++i)
            {
                _matricesBuffer[i].Release();
                _matrices[i].Dispose();
            }

            _matricesBuffer = null;
            _matrices = null;
        }

        public void Init(
            in Vector2Int size,
            in Vector2 cellSize,
            in Color color0,
            in Color color1,
            in Color selectedColor)
        {
            _size = size;
            _cellSize = cellSize;
            _color0 = color0;
            _color1 = color1;
            _selectColor = selectedColor;
            _isInited = true;
            if (gameObject.activeInHierarchy)
                OnEnable();
        }


        public void Select(Vector2Int[] indices)
        {
            Deselect();
            var length = indices.Length;
            if (length <= 0)
                return;

            const int stride = 16 * 4;

            _selectMatrixBuffer = new ComputeBuffer(length, stride);
            _selectMatrix = new NativeArray<Matrix4x4>(length, Allocator.Persistent);

            var tran = transform;
            var position = tran.position - new Vector3(
                0.5f * (_size.x - 1) * _cellSize.x, 0f,
                0.5f * (_size.y - 1) * _cellSize.y);
            var rotation = tran.rotation;
            var scale = tran.lossyScale;
            for (var i = 0; i < length; ++i)
            {
                var x = indices[i].x * _cellSize.x;
                var y = indices[i].y * _cellSize.y;
                var pos = position + new Vector3(x, -0.5f, y);
                _selectMatrix[i] = Matrix4x4.TRS(pos, rotation, scale);
            }

            _selectMatrixBuffer.SetData(_selectMatrix);
        }

        public void Deselect()
        {
            if (_selectMatrixBuffer == null)
                return;

            _selectMatrixBuffer.Dispose();
            _selectMatrix.Dispose();
            _selectMatrixBuffer = null;
        }
    }
}