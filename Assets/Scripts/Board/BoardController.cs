﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEngine;
using UnityEngine.Assertions;
using Color = UnityEngine.Color;

namespace Board
{
    public sealed class BoardController : MonoBehaviour
    {
        [SerializeField] private BoardRender _renderer = null;
        [SerializeField] private BoxCollider _collider = null;

        private readonly Dictionary<Vector2Int, Character> _characters
            = new Dictionary<Vector2Int, Character>();

        private readonly Dictionary<Vector2Int, Vector2Int[]> _cache =
            new Dictionary<Vector2Int, Vector2Int[]>();

        private CharacterBuilder _characterBuilder;
        private ChessEngine _engine;
        private Vector2 _cellSize;
        private Color _color0;
        private Color _color1;
        private Color _selectedColor;

        private bool _hasPossibleMove;

        public event Action<Vector2Int> PawnTransformationReached;

        public void Init(
            CharacterBuilder characterBuilder,
            ChessEngine engine,
            in Vector2 cellSize,
            in Color color0,
            in Color color1,
            in Color selectedColor)
        {
            _characterBuilder = characterBuilder;
            _engine = engine;
            _cellSize = cellSize;
            _color0 = color0;
            _color1 = color1;
            _selectedColor = selectedColor;
        
            _renderer.Init(engine.Size, cellSize, color0, color1, selectedColor);
            _collider.size = new Vector3(
                engine.Size.x * cellSize.x, _collider.size.y, engine.Size.y * cellSize.y);

            foreach (var (index, characterType, teamType) in _engine.GetAllCharacters())
                SetCharacterWithoutRecord(index, characterType, teamType);
        }

        public void Restart(ChessEngine engine)
        {
            _engine = engine;
        
            _renderer.Init(engine.Size, _cellSize, _color0, _color1, _selectedColor);
            _collider.size = new Vector3(
                engine.Size.x * _cellSize.x, _collider.size.y, engine.Size.y * _cellSize.y);
        
            foreach (var character in _characters.Values)
            {
                Destroy(character.gameObject);
            }
            _characters.Clear();
        
            foreach (var (index, characterType, teamType) in _engine.GetAllCharacters())
                SetCharacterWithoutRecord(index, characterType, teamType);
        
            _cache.Clear();
            _hasPossibleMove = false;
        }

        public void UpdateMoveCache(TeamType team)
        {
            _cache.Clear();
            _hasPossibleMove = false;

            foreach (var index in _characters.Keys)
            {
                if (_engine.GetTeam(index) == team)
                {
                    var possibleMoves = _engine.GetPossibleMoves(index).ToArray();
                    _cache[index] = possibleMoves;
                    _hasPossibleMove |= possibleMoves.Length > 0;
                }
            }
        }

        public void Move(in Vector2Int from, in Vector2Int to)
        {
            Assert.IsTrue(_characters.ContainsKey(from));

            Assert.IsTrue(0 <= from.x && from.x < _engine.Size.x && 0 <= from.y &&
                          from.y < _engine.Size.y);
            Assert.IsTrue(0 <= to.x && to.x < _engine.Size.x && 0 <= to.y && to.y < _engine.Size.y);

            if (_engine.IsCastlingMove(from, to))
            {
                if (from.y > to.y)
                    Move(new Vector2Int(from.x, 0), new Vector2Int(from.x, from.y - 1));
                else
                    Move(new Vector2Int(from.x, _engine.Size.y - 1),
                        new Vector2Int(from.x, from.y + 1));
            }

            if (_engine.IsDirtyDestroyMove(from, to))
            {
                var temp = to;

                temp.x += from.x - to.x;
                _engine.Remove(temp);
                if (_characters.ContainsKey(temp))
                {
                    Destroy(_characters[temp].gameObject);
                    _characters.Remove(temp);
                }
                else
                {
                    Debug.LogError("Not valid case!!!");
                }
            }

            _engine.Move(from, to);

            _characters[from].transform.position = GetPosition(to);

            if (_characters.ContainsKey(to))
                Destroy(_characters[to].gameObject);

            _characters[to] = _characters[from];
            _characters.Remove(from);

            if (_engine.IsPawnTransformationMove(to))
                PawnTransformationReached?.Invoke(to);
        }

        public void SetCharacter(
            in Vector2Int index,
            CharacterType characterType,
            TeamType teamType,
            bool isUntapped = false,
            bool isDirty = false)
        {
            SetCharacterWithoutRecord(index, characterType, teamType);
            _engine.SetCharacter(index, characterType, teamType, isUntapped, isDirty);
        }

        public bool TryGetTeam(in Vector2Int index, out TeamType team)
        {
            team = _engine.GetTeam(index);
            return _characters.ContainsKey(index);
        }

        public bool HasPossibleMove() => _hasPossibleMove;

        public bool IsPossibleMove(in Vector2Int from, in Vector2Int to)
        {
            Assert.IsTrue(0 <= from.x && from.x < _engine.Size.x && 0 <= from.y &&
                          from.y < _engine.Size.y);
            Assert.IsTrue(0 <= to.x && to.x < _engine.Size.x && 0 <= to.y && to.y < _engine.Size.y);

            foreach (var move in _cache[from])
            {
                if (move == to)
                    return true;
            }

            return false;
        }

        public Vector2Int GetIndex(in Vector3 position)
        {
            var d = position - transform.position;
            d = new Vector3(d.x / _cellSize.x, 0f, d.z / _cellSize.y);
            var offset = 0.5f * _cellSize * _engine.Size;
            var x = d.x + offset.x;
            var y = d.z + offset.y;
            return new Vector2Int((int) x, (int) y);
        }

        public void SelectPossibleMoves(in Vector2Int index)
        {
            Assert.IsTrue(0 <= index.x 
                          && index.x < _engine.Size.x 
                          && 0 <= index.y 
                          && index.y < _engine.Size.y);

            _renderer.Select(_cache[index]);
        }

        public void Deselect()
        {
            _renderer.Deselect();
        }

        private void SetCharacterWithoutRecord(
            in Vector2Int index, CharacterType characterType, TeamType teamType)
        {
            var character = _characterBuilder
                .SetCharacter(characterType)
                .SetTeam(teamType)
                .Build();

            if (_characters.ContainsKey(index))
                Destroy(_characters[index].gameObject);

            _characters[index] = character;
            character.transform.position = GetPosition(index);
        }

        private Vector3 GetPosition(in Vector2Int index)
        {
            var x = index.x * _cellSize.x;
            var y = index.y * _cellSize.y;
            var offset = _engine.Size - Vector2.one;
            offset *= 0.5f * _cellSize;
            return transform.position + new Vector3(x - offset.x, 0f, y - offset.y);
        }
    }
}