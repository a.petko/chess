﻿using System.Collections.Generic;
using Board;
using Core;
using FsmSystem;
using FsmSystem.Main;
using UI;
using UnityEngine;

namespace Scenes
{
    public sealed class MainScene : MonoBehaviour
    {
        [SerializeField] private DebugBoard _debugBoard = null;
        [SerializeField] private Camera _camera = null;
        [Header("Pages")]
        [SerializeField] private ReturnCharacterPage _returnCharacterPage = null;
        [SerializeField] private WinPage _winPage = null;
        [Header("Builders")]
        [SerializeField] private BoardBuilder _boardBuilder = null;
        [SerializeField] private CharacterBuilder _characterBuilder = null;
    
        private readonly Fsm<State, MainFsmEvent> _fsm = new Fsm<State, MainFsmEvent>(
            comparer: new MainFsmEventIEqualityComparer());

        private TeamType _currentTeam;
        private BoardController _board;
        private GameSession _gameSession;
    
        private void Awake()
        {
            _returnCharacterPage.Close();
            _winPage.Close();
        
            var chessEngine = ChessEngine.GetStartGame(8, 8);

            var teamInfos = new Dictionary<TeamType, TeamSharedInfo>
            {
                [TeamType.White] = 
                    new TeamSharedInfo(Color.white, Quaternion.Euler(0f, 90f, 0f)),
                [TeamType.Black] = 
                    new TeamSharedInfo(Color.cyan, Quaternion.Euler(0f, 270f, 0f)),
            };

            _characterBuilder.SetTeamInfos(teamInfos);
        
            _board = _boardBuilder
                .SetChessEngine(chessEngine)
                .SetCharacterBuilder(_characterBuilder)
                .Build();
        
            _boardBuilder = null;

            _debugBoard.Init(_characterBuilder, chessEngine);

            _gameSession = new GameSession();

            var idleState = new State();
            var player0State = new PlayerState(_camera, TeamType.White, _gameSession, _board);
            var player1State = new PlayerState(_camera, TeamType.Black, _gameSession, _board);
        
            var selectCharacterState = 
                new SelectCharacterState(_returnCharacterPage, _board, _gameSession);
        
            player0State.Moved += () => 
                _fsm.Fire(_gameSession.PawnTransformationReachedIndex.HasValue
                ? MainFsmEvent.PawnTransformation
                : MainFsmEvent.BlackState);
        
            player1State.Moved += () => 
                _fsm.Fire(_gameSession.PawnTransformationReachedIndex.HasValue
                ? MainFsmEvent.PawnTransformation
                : MainFsmEvent.WhiteState);

            player0State.GameOvered += () =>
            {
                _fsm.Fire(MainFsmEvent.Idle);
                _winPage.Open(TeamType.Black);
            };
        
            player1State.GameOvered += () =>
            {
                _fsm.Fire(MainFsmEvent.Idle);
                _winPage.Open(TeamType.White);
            };
        
            selectCharacterState.Completed += () => _fsm.Fire(
                _gameSession.CurrentTeam == TeamType.White 
                    ? MainFsmEvent.BlackState 
                    : MainFsmEvent.WhiteState);
        
            _board.PawnTransformationReached += index 
                => _gameSession.PawnTransformationReachedIndex = index;
        
            _fsm
                .Add(player0State, player1State, 
                    MainFsmEvent.BlackState)
                .Add(player1State, player0State, 
                    MainFsmEvent.WhiteState)
                .Add(player0State, selectCharacterState, 
                    MainFsmEvent.PawnTransformation)
                .Add(player1State, selectCharacterState, 
                    MainFsmEvent.PawnTransformation)
                .Add(selectCharacterState, player0State, 
                    MainFsmEvent.WhiteState)
                .Add(selectCharacterState, player1State, 
                    MainFsmEvent.BlackState)
                .Add(player0State, idleState, 
                    MainFsmEvent.Idle)
                .Add(player1State, idleState, 
                    MainFsmEvent.Idle)
                .Add(idleState, player0State, 
                    MainFsmEvent.WhiteState)
                .Start(player0State);

            _gameSession.CharacterChanged += OnCharacterChanged;

            _winPage.RestartClicked += OnRestart;
        }

        private void Update()
        {
            _fsm.Current.Update();
        }
    
        private void OnCharacterChanged()
        {
            var current = _gameSession.CurrentIndex;
            if (current != null)
                _board.SelectPossibleMoves(current.Value);
            else
                _board.Deselect();
        }
    
        private void OnRestart()
        {
            _winPage.Close();
        
            var chessEngine = ChessEngine.GetStartGame(8, 8);
            _board.Restart(chessEngine);
            _debugBoard.Init(_characterBuilder, chessEngine);

            _gameSession.CurrentIndex = null;
            _gameSession.PawnTransformationReachedIndex = null;
            _fsm.Fire(MainFsmEvent.WhiteState);
        }
    }
}