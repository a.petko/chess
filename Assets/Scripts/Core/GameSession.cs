﻿using System;
using UnityEngine;

namespace Core
{
    public class GameSession
    {
        private int _currentPlayer;

        private Vector2Int? _currentIndex;

        public Vector2Int? CurrentIndex
        {
            get => _currentIndex;
            set
            {
                if (_currentIndex == value)
                    return;

                _currentIndex = value;
                CharacterChanged?.Invoke();
            }
        }

        public Vector2Int? PawnTransformationReachedIndex { get; set; }

        public TeamType CurrentTeam { get; set; }

        public event Action CharacterChanged;
    }
}