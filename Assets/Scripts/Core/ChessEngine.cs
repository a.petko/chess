﻿using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public sealed class ChessEngine
    {
        public readonly Vector2Int Size;

        private const int King = (int) CharacterType.King;
        private const int Queen = (int) CharacterType.Queen;
        private const int Bishop = (int) CharacterType.Bishop;
        private const int Rook = (int) CharacterType.Rook;
        private const int Knight = (int) CharacterType.Knight;
        private const int Pawn = (int) CharacterType.Pawn;
        private const int Untapped = 1 << 6;
        private const int Dirty = 1 << 7;
        private const int White = (int) TeamType.White;
        private const int Black = (int) TeamType.Black;

        private const int CharacterMask = King | Queen | Bishop | Rook | Knight | Pawn;
        private const int TeamMask = White | Black;

        private static readonly Vector2Int[] KnightMoves =
        {
            new Vector2Int(2, 1),
            new Vector2Int(2, -1),
            new Vector2Int(-2, 1),
            new Vector2Int(-2, -1),

            new Vector2Int(1, 2),
            new Vector2Int(1, -2),
            new Vector2Int(-1, 2),
            new Vector2Int(-1, -2),
        };

        private readonly int[][] _board;

        private readonly HashSet<Vector2Int> _dangerousIndices = new HashSet<Vector2Int>();
        private readonly List<Vector2Int> _dangerousLine = new List<Vector2Int>();

        private Vector2Int _dirtyIndex;
        private Vector2Int _whiteKing;
        private Vector2Int _blackKing;

        public ChessEngine(int[][] board)
        {
            _board = board;
            Size = new Vector2Int(_board.Length, _board[0].Length);
            for (var x = 0; x < Size.x; ++x)
            for (var y = 0; y < Size.y; ++y)
            {
                var num = _board[x][y];
                if ((num & King) != King)
                    continue;

                if ((num & White) == White)
                    _whiteKing = new Vector2Int(x, y);
                else
                    _blackKing = new Vector2Int(x, y);
            }
        }

        public static ChessEngine GetStartGame(int x, int y)
        {
            var board = new int[x][];
            for (var i = 0; i < x; ++i)
                board[i] = new int[y];

            for (var i = 0; i < y; ++i)
            {
                board[1][i] = Pawn | White | Untapped;
                board[x - 2][i] = Pawn | Black | Untapped;
            }

            board[0][0] = Rook | White | Untapped;
            board[0][y - 1] = Rook | White | Untapped;
            board[x - 1][0] = Rook | Black | Untapped;
            board[x - 1][y - 1] = Rook | Black | Untapped;

            board[0][1] = Knight | White | Untapped;
            board[0][y - 2] = Knight | White | Untapped;
            board[x - 1][1] = Knight | Black | Untapped;
            board[x - 1][y - 2] = Knight | Black | Untapped;

            board[0][2] = Bishop | White | Untapped;
            board[0][y - 3] = Bishop | White | Untapped;
            board[x - 1][2] = Bishop | Black | Untapped;
            board[x - 1][y - 3] = Bishop | Black | Untapped;

            board[0][y - 4] = Queen | White | Untapped;
            board[x - 1][y - 4] = Queen | Black | Untapped;

            board[0][3] = King | White | Untapped;
            board[x - 1][3] = King | Black | Untapped;

            return new ChessEngine(board);
        }

        public TeamType GetTeam(in Vector2Int index) =>
            (_board[index.x][index.y] & White) == White ? TeamType.White : TeamType.Black;

        public IEnumerable<(Vector2Int, CharacterType, TeamType)> GetAllCharacters()
        {
            for (var x = 0; x < Size.x; ++x)
            for (var y = 0; y < Size.y; ++y)
            {
                var num = _board[x][y];
                if (num == 0)
                    continue;

                yield return (
                    new Vector2Int(x, y),
                    (CharacterType) (num & CharacterMask),
                    (num & White) == White ? TeamType.White : TeamType.Black
                );
            }
        }

        public IEnumerable<Vector2Int> GetPossibleMoves(Vector2Int index, bool isTest = false)
        {
            var num = _board[index.x][index.y];
            if (num == 0)
                yield break;

            if ((num & Pawn) == Pawn)
            {
                var i = index;
                var dir = (num & White) == White ? 1 : -1;
                i.x += dir;
                if (!isTest && IsBoard(i) && IsFree(i) && IsKingSelf(i))
                    yield return i;

                ++i.y;
                if (IsBoard(i) && IsKingSelf(i))
                {
                    var temp = i;
                    temp.x -= dir;

                    var numTemp = _board[temp.x][temp.y];
                    _board[temp.x][temp.y] = 0;

                    if ((IsEnemy(i)
                         || IsFree(i)
                         && (numTemp & Dirty) == Dirty)
                        && IsKingSelf(i)
                    )
                        yield return i;

                    _board[temp.x][temp.y] = numTemp;
                }

                i.y -= 2;
                if (IsBoard(i) && IsKingSelf(i))
                {
                    var temp = i;
                    temp.x -= dir;

                    var numTemp = _board[temp.x][temp.y];
                    _board[temp.x][temp.y] = 0;

                    if (IsEnemy(i)
                        || IsFree(i)
                        && (numTemp & Dirty) == Dirty
                        && IsKingSelf(i)
                    )
                        yield return i;

                    _board[temp.x][temp.y] = numTemp;
                }

                ++i.y;
                if (!isTest && (num & Untapped) == Untapped)
                {
                    var temp = i;
                    i.x += dir;
                    if (IsBoard(i) && IsFree(i) && IsFree(temp) && IsKingSelf(i))
                        yield return i;
                }

                yield break;
            }

            if ((num & King) == King)
            {
                var enemyPossibleMoves = new HashSet<Vector2Int>();
                var numTeam = num & TeamMask;
                if (!isTest)
                {
                    for (var x = 0; x < Size.x; ++x)
                    for (var y = 0; y < Size.y; ++y)
                        if ((_board[x][y] & numTeam) == 0)
                            enemyPossibleMoves.UnionWith(GetPossibleMoves(new Vector2Int(x, y),
                                true));
                }

                var i = new Vector2Int(index.x - 1, index.y - 1);
                for (; i.x <= index.x + 1; ++i.x)
                for (i.y = index.y - 1; i.y <= index.y + 1; ++i.y)
                {
                    if (isTest || IsFreeOrEnemy(i) && !enemyPossibleMoves.Contains(i))
                        yield return i;
                }

                if ((num & Untapped) != Untapped)
                    yield break;

                if ((_board[index.x][Size.y - 1] & Untapped) == Untapped)
                {
                    var isEmpty = true;
                    for (var y = index.y + 1; y < Size.y - 1; ++y)
                    {
                        if (_board[index.x][y] != 0)
                        {
                            isEmpty = false;
                            break;
                        }
                    }

                    if (isEmpty
                        && !enemyPossibleMoves.Contains(new Vector2Int(index.x, index.y + 1))
                        && !enemyPossibleMoves.Contains(new Vector2Int(index.x, index.y + 2))
                    )
                        yield return new Vector2Int(index.x, index.y + 2);
                }

                if ((_board[index.x][0] & Untapped) == Untapped)
                {
                    var isEmpty = true;
                    for (var y = index.y - 1; 0 < y; --y)
                    {
                        if (_board[index.x][y] != 0)
                        {
                            isEmpty = false;
                            break;
                        }
                    }

                    if (isEmpty
                        && !enemyPossibleMoves.Contains(new Vector2Int(index.x, index.y - 1))
                        && !enemyPossibleMoves.Contains(new Vector2Int(index.x, index.y - 2))
                    )
                        yield return new Vector2Int(index.x, index.y - 2);
                }

                yield break;
            }

            if ((num & Queen) == Queen)
            {
                for (var i = new Vector2Int(index.x + 1, index.y); i.x < Size.x; ++i.x)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x - 1, index.y); 0 <= i.x; --i.x)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x, index.y + 1); i.y < Size.y; ++i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x, index.y - 1); 0 <= i.y; --i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x + 1, index.y + 1);
                    i.x < Size.x && i.y < Size.y;
                    ++i.x, ++i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x - 1, index.y - 1);
                    0 <= i.x && 0 <= i.y;
                    --i.x, --i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x + 1, index.y - 1);
                    i.x < Size.x && 0 <= i.y;
                    ++i.x, --i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x - 1, index.y + 1);
                    0 <= i.x && i.y < Size.y;
                    --i.x, ++i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                yield break;
            }

            if ((num & Bishop) == Bishop)
            {
                for (var i = new Vector2Int(index.x + 1, index.y + 1);
                    i.x < Size.x && i.y < Size.y;
                    ++i.x, ++i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x - 1, index.y - 1);
                    0 <= i.x && 0 <= i.y;
                    --i.x, --i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x + 1, index.y - 1);
                    i.x < Size.x && 0 <= i.y;
                    ++i.x, --i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x - 1, index.y + 1);
                    0 <= i.x && i.y < Size.y;
                    --i.x, ++i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                yield break;
            }

            if ((num & Rook) == Rook)
            {
                for (var i = new Vector2Int(index.x + 1, index.y); i.x < Size.x; ++i.x)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x - 1, index.y); 0 <= i.x; --i.x)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x, index.y + 1); i.y < Size.y; ++i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                for (var i = new Vector2Int(index.x, index.y - 1); 0 <= i.y; --i.y)
                {
                    if (!IsBoard(i))
                        break;

                    if (IsFree(i))
                    {
                        if (IsKingSelf(i))
                            yield return i;
                        continue;
                    }

                    if (IsEnemy(i) && IsKingSelf(i))
                        yield return i;

                    if (!IsEnemyKingOnlyTestMode(i))
                        break;
                }

                yield break;
            }

            if ((num & Knight) == Knight)
            {
                foreach (var move in KnightMoves)
                {
                    var i = index + move;
                    if (IsFreeOrEnemy(i) && IsKingSelf(i))
                        yield return i;
                }

                yield break;
            }

            bool IsKingSelf(in Vector2Int i0)
            {
                if (isTest)
                    return true;

                var isSelf = true;
                var temp = _board[index.x][index.y];
                var i0Temp = _board[i0.x][i0.y];

                _board[index.x][index.y] = 0;
                _board[i0.x][i0.y] = temp;

                if (GetMandatoryMoves(temp, _dangerousIndices))
                    isSelf = false;

                _board[index.x][index.y] = temp;
                _board[i0.x][i0.y] = i0Temp;

                return isSelf;
            }

            bool IsEnemy(in Vector2Int i)
            {
                if (isTest)
                    return true;

                var other = _board[i.x][i.y];
                return other != 0 && (num & TeamMask) != (other & TeamMask);
            }

            bool IsFreeOrEnemy(in Vector2Int i)
            {
                if (!IsBoard(i))
                    return false;

                var other = _board[i.x][i.y];
                return isTest || other == 0 || (num & TeamMask) != (other & TeamMask);
            }

            bool IsEnemyKingOnlyTestMode(in Vector2Int i)
            {
                var other = _board[i.x][i.y];
                return isTest && (other & King) == King && (num & other & TeamMask) == 0;
            }
        }

        private void GetDangerousLine(
            int num, Vector2Int index, in Vector2Int addedIndex, int mask, List<Vector2Int> list,
            int
                lenght = int.MaxValue)
        {
            list.Clear();
            var numTeam = num & TeamMask;

            var counter = 0;
            for (var i = index + addedIndex;
                0 <= i.x && i.x < Size.x && 0 <= i.y && i.y < Size.y && counter < lenght;
                i += addedIndex, ++counter)
            {
                var n = _board[i.x][i.y];

                if (n == 0)
                {
                    list.Add(i);
                    continue;
                }

                if ((n & numTeam) == 0 && (n & mask) != 0)
                {
                    list.Add(i);
                    return;
                }

                break;
            }

            list.Clear();
        }

        private bool GetMandatoryMoves(int num, HashSet<Vector2Int> mandatoryMoves)
        {
            mandatoryMoves.Clear();
            var isWhite = (num & White) == White;
            var index = isWhite ? _whiteKing : _blackKing;
            var isInit = false;

            var mask = Rook | Queen;

            CheckLine(1, 0);
            CheckLine(-1, 0);
            CheckLine(0, 1);
            CheckLine(0, -1);

            mask = Bishop | Queen;

            CheckLine(1, 1);
            CheckLine(1, -1);


            CheckLine(-1, 1);
            CheckLine(-1, -1);

            CheckKnights();

            mask = Pawn;
            var pawnDir = isWhite ? 1 : -1;
            CheckLine(pawnDir, 1, 1);
            CheckLine(pawnDir, -1, 1);

            return isInit;

            void CheckLine(int x, int y, int length = int.MaxValue)
            {
                GetDangerousLine(
                    num, index, new Vector2Int(x, y), mask, _dangerousLine, length);

                if (_dangerousLine.Count <= 0)
                    return;

                if (isInit)
                    mandatoryMoves.IntersectWith(_dangerousLine);
                else
                {
                    mandatoryMoves.UnionWith(_dangerousLine);
                    isInit = true;
                }
            }

            void CheckKnights()
            {
                _dangerousLine.Clear();
                foreach (var move in KnightMoves)
                {
                    var i = index + move;
                    if (!IsBoard(i))
                        continue;

                    var n = _board[i.x][i.y];
                    if ((n & Knight) == Knight && (num & n & TeamMask) == 0)
                        _dangerousLine.Add(i);
                }

                if (_dangerousLine.Count <= 0)
                    return;

                if (isInit)
                    mandatoryMoves.IntersectWith(_dangerousLine);
                else
                {
                    mandatoryMoves.UnionWith(_dangerousLine);
                    isInit = true;
                }
            }
        }

        public void SetCharacter(
            in Vector2Int index,
            CharacterType character,
            TeamType team,
            bool isUntapped = false,
            bool isDirty = false)
        {
            var num = (int) character | (int) team;

            if (isUntapped)
                num |= Untapped;

            if (isDirty)
                num |= Dirty;

            _board[index.x][index.y] = num;
        }

        public bool IsCastlingMove(in Vector2Int from, in Vector2Int to)
        {
            var fromNum = _board[from.x][from.y];
            if ((fromNum & King) != King)
                return false;

            var delta = from.y - to.y;
            return delta == 2 || delta == -2;
        }

        public bool IsDirtyDestroyMove(in Vector2Int from, in Vector2Int to)
        {
            var fromNum = _board[from.x][from.y];
            if ((fromNum & Pawn) != Pawn)
                return false;

            return (_board[to.x + from.x - to.x][to.y] & Dirty) == Dirty;
        }

        public bool IsPawnTransformationMove(in Vector2Int to)
        {
            var num = _board[to.x][to.y];
            return (num & Pawn) == Pawn && (to.x == 0 || to.x == Size.x - 1);
        }

        public void Remove(in Vector2Int index)
        {
            _board[index.x][index.y] = 0;
        }

        public void Move(in Vector2Int from, in Vector2Int to)
        {
            _board[_dirtyIndex.x][_dirtyIndex.y] &= ~Dirty;

            var fromNum = _board[from.x][from.y];

            _board[to.x][to.y] = fromNum & ~Untapped;
            _board[from.x][from.y] = 0;

            if ((fromNum & Pawn) == Pawn)
            {
                var delta = from.x - to.x;
                if (delta == 2 || delta == -2)
                {
                    _board[to.x][to.y] |= Dirty;
                    _dirtyIndex = to;
                }
            }
            else if ((fromNum & King) == King)
            {
                if ((fromNum & White) == White)
                    _whiteKing = to;
                else
                    _blackKing = to;
            }
        }

        private bool IsFree(in Vector2Int index)
        {
            var other = _board[index.x][index.y];
            return other == 0;
        }

        private bool IsBoard(in Vector2Int index) =>
            0 <= index.x && index.x < Size.x && 0 <= index.y && index.y < Size.y;
    }
}