﻿using System;

namespace Core
{
    [Flags]
    public enum TeamType
    {
        White = 1 << 8,
        Black = 1 << 9,
    }
}