﻿namespace Core
{
    public enum CharacterType
    {
        King = 1 << 0,
        Queen = 1 << 1,
        Bishop = 1 << 2,
        Rook = 1 << 3,
        Knight = 1 << 4,
        Pawn = 1 << 5,
    }
}